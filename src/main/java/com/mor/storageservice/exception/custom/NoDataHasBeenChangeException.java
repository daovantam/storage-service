package com.mor.storageservice.exception.custom;

import com.mor.storageservice.constant.ResponseStatusCodeEnum;
import lombok.Getter;

@Getter
public class NoDataHasBeenChangeException extends RuntimeException {

  private final String code;

  public NoDataHasBeenChangeException(ResponseStatusCodeEnum massage) {
    super(massage.getMessage());
    this.code = massage.getCode();
  }
}
