package com.mor.storageservice.controller;

import com.mor.storageservice.dto.response.FileResponse;
import com.mor.storageservice.dto.search.SearchCriteriaDto;
import com.mor.storageservice.dto.search.SearchDtoResponse;
import com.mor.storageservice.entity.FileEntity;
import com.mor.storageservice.factory.ResponseFactory;
import com.mor.storageservice.service.UploadService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import java.io.IOException;
import java.security.Principal;
import java.util.List;
import javax.validation.Valid;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping("api/v1/upload")
@Slf4j
public class UploadController {

  private final UploadService uploadService;

  private final ResponseFactory responseFactory;

  @Autowired
  public UploadController(UploadService uploadService,
      ResponseFactory responseFactory) {
    this.uploadService = uploadService;
    this.responseFactory = responseFactory;
  }

  @PostMapping
  public ResponseEntity<Object> uploadImages(@RequestParam("images") List<MultipartFile> images,
      Principal principal) throws IOException {
    log.info("start uploadImages");
    List<FileResponse> response = uploadService.uploadToS3(images, principal);

    return responseFactory.success(response, List.class);
  }

  @ApiOperation(value = "search Criteria File", response = SearchDtoResponse.class)
  @ApiResponses(value = {
      @ApiResponse(code = 200, message = "Successfully search Criteria File"),
      @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
      @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
      @ApiResponse(code = 404, message = "The resource you were trying to reach is not found"),
      @ApiResponse(code = 400, message = "Invalid params, please try again")})
  @PostMapping("/search")
  @PreAuthorize("hasRole('ROLE_ADMIN') || hasRole('ROLE_RM')")
  public ResponseEntity<Object> search(@RequestBody @Valid SearchCriteriaDto searchCriteriaDto) {
    SearchDtoResponse<FileResponse> dtoResponse = uploadService.search(searchCriteriaDto);
    return responseFactory.success(dtoResponse, SearchDtoResponse.class);
  }

  @ApiOperation(value = "search Criteria File", response = SearchDtoResponse.class)
  @ApiResponses(value = {
      @ApiResponse(code = 200, message = "Successfully search Criteria File"),
      @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
      @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
      @ApiResponse(code = 404, message = "The resource you were trying to reach is not found"),
      @ApiResponse(code = 400, message = "Invalid params, please try again")})
  @PostMapping("/host-search")
  public ResponseEntity<Object> searchHost(@RequestBody @Valid SearchCriteriaDto searchCriteriaDto, Principal principal) {
    SearchDtoResponse<FileResponse> dtoResponse = uploadService.searchHost(searchCriteriaDto, principal);
    return responseFactory.success(dtoResponse, SearchDtoResponse.class);
  }

  @ApiOperation(value = "delete File")
  @ApiResponses(value = {
      @ApiResponse(code = 200, message = "Successfully delete File"),
      @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
      @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
      @ApiResponse(code = 404, message = "The resource you were trying to reach is not found"),
      @ApiResponse(code = 400, message = "Invalid params, please try again")})
  @DeleteMapping
  public ResponseEntity<Object> delete(@RequestParam("path") String path) {
    uploadService.deleteImage(path);

    return responseFactory.success();
  }

  @ApiOperation(value = "get File", response = FileResponse.class)
  @ApiResponses(value = {
      @ApiResponse(code = 200, message = "Successfully get File"),
      @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
      @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
      @ApiResponse(code = 404, message = "The resource you were trying to reach is not found"),
      @ApiResponse(code = 400, message = "Invalid params, please try again")})
  @GetMapping
  public ResponseEntity<Object> get(@RequestParam("url") String url) {
    FileResponse fileResponse = uploadService.getImage(url);

    return responseFactory.success(fileResponse, FileResponse.class);
  }
}
