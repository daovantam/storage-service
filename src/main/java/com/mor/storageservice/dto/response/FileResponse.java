package com.mor.storageservice.dto.response;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class FileResponse {

  private String url;

  private String name;

  private String path;
}
