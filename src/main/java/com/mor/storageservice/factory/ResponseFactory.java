package com.mor.storageservice.factory;

import com.mor.storageservice.constant.ResponseStatusCodeEnum;
import java.util.Date;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

@Component
public class ResponseFactory {

  public ResponseEntity<Object> success(Object data, Class<?> clazz) {
    GeneralResponse<Object> responseObject = new GeneralResponse<>();
    ResponseStatus responseStatus = new ResponseStatus();
    responseStatus.setCode(ResponseStatusCodeEnum.SUCCESS.getCode());
    responseStatus.setMessage(ResponseStatusCodeEnum.SUCCESS.getMessage());
    responseObject.setStatus(responseStatus);
    responseObject.setData(clazz.cast(data));
    responseObject.setTimestamp(new Date());

    return ResponseEntity.ok(responseObject);
  }

  public ResponseEntity<Object> success() {
    GeneralResponse<Object> responseObject = new GeneralResponse<>();
    ResponseStatus responseStatus = new ResponseStatus();
    responseStatus.setCode(ResponseStatusCodeEnum.SUCCESS.getCode());
    responseStatus.setMessage(ResponseStatusCodeEnum.SUCCESS.getMessage());
    responseObject.setStatus(responseStatus);
    responseObject.setTimestamp(new Date());
    responseObject.setData("SUCCESS");
    return ResponseEntity.ok(responseObject);
  }

  public ResponseEntity<Object> created(Object data, Class<?> clazz) {
    GeneralResponse<Object> responseObject = new GeneralResponse<>();
    ResponseStatus responseStatus = new ResponseStatus();
    responseStatus.setCode(ResponseStatusCodeEnum.CREATED.getCode());
    responseStatus.setMessage(ResponseStatusCodeEnum.CREATED.getMessage());
    responseObject.setStatus(responseStatus);
    responseObject.setData(clazz.cast(data));
    responseObject.setTimestamp(new Date());

    return new ResponseEntity<>(responseObject, HttpStatus.CREATED);
  }
}
