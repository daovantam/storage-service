package com.mor.storageservice.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "file")
@Getter
@Setter
public class FileEntity extends BaseEntity{

  @Id
  @Column(name = "url")
  private String url;

  @Column(name = "name")
  private String name;

  @Column(name = "path")
  private String path;
}
