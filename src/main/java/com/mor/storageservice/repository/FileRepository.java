package com.mor.storageservice.repository;

import com.mor.storageservice.entity.FileEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface FileRepository extends JpaRepository<FileEntity, String>,
    JpaSpecificationExecutor<FileEntity> {
  void deleteByPath(String path);
}
