package com.mor.storageservice.utils;

import com.mor.storageservice.dto.response.FileResponse;
import com.mor.storageservice.entity.FileEntity;
import org.springframework.stereotype.Component;

@Component
public class FileConverter {

  public FileResponse fileResponse(FileEntity fileEntity) {
    FileResponse fileResponse = new FileResponse();
    fileResponse.setName(fileEntity.getName());
    fileResponse.setPath(fileEntity.getPath());
    fileResponse.setUrl(fileEntity.getUrl());

    return fileResponse;
  }
}
