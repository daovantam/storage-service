package com.mor.storageservice.service;

import com.mor.storageservice.dto.response.FileResponse;
import com.mor.storageservice.dto.search.SearchCriteriaDto;
import com.mor.storageservice.dto.search.SearchDtoResponse;
import java.io.IOException;
import java.security.Principal;
import java.util.List;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

@Service
public interface UploadService {

  List<FileResponse> uploadToS3(List<MultipartFile> multipartFiles, Principal principal)
      throws IOException;
  SearchDtoResponse<FileResponse> search(SearchCriteriaDto searchCriteriaDto);
  SearchDtoResponse<FileResponse> searchHost(SearchCriteriaDto searchCriteriaDto, Principal principal);
  void deleteImage(String path);

  FileResponse getImage(String url);
}
