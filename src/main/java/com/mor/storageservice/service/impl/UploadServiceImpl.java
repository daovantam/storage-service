package com.mor.storageservice.service.impl;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.DeleteObjectRequest;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.mor.storageservice.dto.response.FileResponse;
import com.mor.storageservice.dto.search.SearchCriteria;
import com.mor.storageservice.dto.search.SearchCriteriaDto;
import com.mor.storageservice.dto.search.SearchDtoResponse;
import com.mor.storageservice.dto.search.SearchPreProcess;
import com.mor.storageservice.entity.FileEntity;
import com.mor.storageservice.repository.FileRepository;
import com.mor.storageservice.search.SpecificationBuilder;
import com.mor.storageservice.service.SearchService;
import com.mor.storageservice.service.UploadService;
import com.mor.storageservice.utils.FileConverter;
import java.io.File;
import java.io.IOException;
import java.security.Principal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import javax.persistence.EntityNotFoundException;
import javax.transaction.Transactional;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

@Service
@Slf4j
@Transactional
public class UploadServiceImpl implements UploadService {

  private final AmazonS3 amazonS3;
  private final FileRepository fileRepository;
  private final SearchService<FileEntity> searchService;
  private final FileConverter fileConverter;
  private final SpecificationBuilder builder = new SpecificationBuilder(FileEntity.class);
  private final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
  private final SimpleDateFormat simpleTime = new SimpleDateFormat("hh:mm:ss");
  @Value("${cloud.aws.bucketName}")
  private String bucketName;

  @Autowired
  public UploadServiceImpl(AmazonS3 amazonS3,
      FileRepository fileRepository,
      SearchService<FileEntity> searchService,
      FileConverter fileConverter) {
    this.amazonS3 = amazonS3;
    this.fileRepository = fileRepository;
    this.searchService = searchService;
    this.fileConverter = fileConverter;
  }

  @Override
  public List<FileResponse> uploadToS3(List<MultipartFile> multipartFiles, Principal principal)
      throws IOException {
    List<FileResponse> fileEntities = new ArrayList<>();
    Date current = new Date();
    for (MultipartFile multipartFile : multipartFiles) {
      File file = new File("/images/" + multipartFile.getOriginalFilename());
      multipartFile.transferTo(file);
      String objectOnS3 = principal.getName() + "/" + simpleDateFormat.format(current) + "/" +
          simpleTime.format(current) + "-" + file.getName();
      PutObjectRequest request = new PutObjectRequest(bucketName, objectOnS3, file)
          .withCannedAcl(CannedAccessControlList.PublicRead);

      amazonS3.putObject(request);

      log.info("upload file with name {} done.", file.getName());

      if (file.delete()) {
        log.info("deleted {} in local", file.getName());
      }

      String url = amazonS3.getUrl(bucketName, objectOnS3)
          .toString();

      FileEntity fileEntity = new FileEntity();
      fileEntity.setPath(objectOnS3);
      fileEntity.setName(file.getName());
      fileEntity.setUrl(url);
      fileEntity.setCreatedBy(principal.getName());
      fileEntity.setUpdatedBy(principal.getName());
      FileEntity uploaded = fileRepository.save(fileEntity);
      fileEntities.add(fileConverter.fileResponse(uploaded));
    }
    return fileEntities;
  }

  @Override
  public SearchDtoResponse<FileResponse> search(SearchCriteriaDto searchCriteriaDto) {
    SearchDtoResponse<FileResponse> searchDtoResponse = new SearchDtoResponse<>();
    SearchPreProcess<FileEntity> provinceSearchPreProcess = searchService
        .search(searchCriteriaDto, builder);
    Page<FileEntity> wards = fileRepository.findAll(provinceSearchPreProcess.getSpecification(),
        provinceSearchPreProcess.getPageable());
    List<FileResponse> provinceResponseDtoList = wards.getContent().stream().parallel()
        .map(fileConverter::fileResponse).collect(Collectors.toList());
    searchDtoResponse.setCurrentPage(searchCriteriaDto.getPage());
    searchDtoResponse.setPageSize(searchCriteriaDto.getPageSize());
    searchDtoResponse.setTotalPage(wards.getTotalPages());
    searchDtoResponse.setTotalElement(wards.getTotalElements());
    searchDtoResponse.setListObject(provinceResponseDtoList);
    return searchDtoResponse;
  }

  @Override
  public SearchDtoResponse<FileResponse> searchHost(SearchCriteriaDto searchCriteriaDto, Principal principal) {
    SearchCriteria searchCriteria = new SearchCriteria();
    searchCriteria.setKey("createdBy");
    searchCriteria.setOperation(":");
    searchCriteria.setOrPredicate(false);
    searchCriteria.setValue(principal.getName());
    searchCriteriaDto.getMatching().add(searchCriteria);
    return search(searchCriteriaDto);
  }

  @Override
  public void deleteImage(String path) {
    amazonS3.deleteObject(new DeleteObjectRequest(bucketName, path));
    fileRepository.deleteByPath(path);
  }

  @Override
  public FileResponse getImage(String url) {
    FileEntity fileEntity = fileRepository.findById(url)
        .orElseThrow(() -> new EntityNotFoundException("Image not found"));
    return fileConverter.fileResponse(fileEntity);
  }
}
