package com.mor.storageservice.service;

import com.mor.storageservice.dto.search.SearchCriteriaDto;
import com.mor.storageservice.dto.search.SearchPreProcess;
import com.mor.storageservice.search.SpecificationBuilder;

public interface SearchService<T> {

  SearchPreProcess<T> search(SearchCriteriaDto searchCriteriaDto, SpecificationBuilder builder);
}
