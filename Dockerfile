FROM openjdk:8
ADD target/storage-service.jar storage-service.jar

RUN mkdir /images
ENTRYPOINT ["java", "-jar", "storage-service.jar"]